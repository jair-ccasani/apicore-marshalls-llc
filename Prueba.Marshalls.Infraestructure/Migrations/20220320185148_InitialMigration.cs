﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Prueba.Marshalls.Infraestructure.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblEmployee",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Year = table.Column<int>(type: "int", nullable: true),
                    Month = table.Column<int>(type: "int", nullable: true),
                    Office = table.Column<string>(type: "varchar(5)", unicode: false, maxLength: 5, nullable: true),
                    EmployeeCode = table.Column<string>(name: "Employee Code", type: "varchar(10)", unicode: false, maxLength: 10, nullable: true),
                    EmployeeName = table.Column<string>(name: "Employee Name", type: "varchar(150)", unicode: false, maxLength: 150, nullable: true),
                    EmployeeSurname = table.Column<string>(name: "Employee Surname", type: "varchar(150)", unicode: false, maxLength: 150, nullable: true),
                    Division = table.Column<string>(type: "varchar(150)", unicode: false, maxLength: 150, nullable: true),
                    Position = table.Column<string>(type: "varchar(150)", unicode: false, maxLength: 150, nullable: true),
                    Grade = table.Column<int>(type: "int", nullable: true),
                    BeginDate = table.Column<DateTime>(name: "Begin Date", type: "date", nullable: true),
                    Birthday = table.Column<DateTime>(type: "date", nullable: true),
                    IdentificationNumber = table.Column<string>(name: "Identification Number", type: "varchar(10)", unicode: false, maxLength: 10, nullable: true),
                    BaseSalary = table.Column<decimal>(name: "Base Salary", type: "decimal(18,0)", nullable: true),
                    ProductionBonus = table.Column<decimal>(name: "Production Bonus", type: "decimal(18,0)", nullable: true),
                    CompensationBonus = table.Column<decimal>(name: "Compensation Bonus", type: "decimal(18,0)", nullable: true),
                    Commission = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    Contributions = table.Column<decimal>(type: "decimal(18,0)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblEmployee", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "tblEmployee",
                columns: new[] { "Id", "Base Salary", "Begin Date", "Birthday", "Commission", "Compensation Bonus", "Contributions", "Division", "Employee Code", "Employee Name", "Employee Surname", "Grade", "Identification Number", "Month", "Office", "Position", "Production Bonus", "Year" },
                values: new object[,]
                {
                    { 1, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 3215m, 1000m, "OPERATION", "10001222", "MIKE", "JAMES", 18, "45642132", 10, "C", "CARGO MANAGER", 0m, 2021 },
                    { 73, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 240m, "OPERATION", "10007365", "SVEN", "PETERSEN", 18, "23654125", 10, "C", "CARGO MANAGER", 4267m, 2021 },
                    { 72, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1909m, 0m, 0m, "CUSTOMER CARE", "10007464", "MARTIN", "BEIN", 6, "98546258", 5, "A", "CUSTOMER ASSISTANT", 4061m, 2021 },
                    { 71, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1515m, 669m, 1021m, "CUSTOMER CARE", "10007464", "MARTIN", "BEIN", 6, "98546258", 6, "A", "CUSTOMER ASSISTANT", 0m, 2021 },
                    { 70, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 82m, 1215m, 1015m, "CUSTOMER CARE", "10007464", "MARTIN", "BEIN", 14, "98546258", 7, "D", "CUSTOMER DIRECTOR", 3649m, 2021 },
                    { 69, 3200m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1301m, 0m, 0m, "CUSTOMER CARE", "10007464", "MARTIN", "BEIN", 14, "98546258", 8, "D", "CUSTOMER DIRECTOR", 3443m, 2021 },
                    { 68, 5000m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1160m, 475m, 68m, "CUSTOMER CARE", "10007464", "MARTIN", "BEIN", 18, "98546258", 9, "D", "CUSTOMER DIRECTOR", 0m, 2021 },
                    { 67, 5000m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 161m, 1687m, "CUSTOMER CARE", "10007464", "MARTIN", "BEIN", 18, "98546258", 10, "D", "CUSTOMER DIRECTOR", 3031m, 2021 },
                    { 66, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 1394m, 0m, 810m, "MARKETING", "10007562", "PETRA", "WINKLER", 11, "45698756", 5, "ZZ", "MARKETING ASSISTANT", 2825m, 2021 },
                    { 65, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 1869m, 1032m, 740m, "MARKETING", "10007562", "PETRA", "WINKLER", 11, "45698756", 6, "ZZ", "MARKETING ASSISTANT", 0m, 2021 },
                    { 64, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 0m, "SALES", "10007562", "PETRA", "WINKLER", 12, "45698756", 7, "ZZ", "ACCOUNT EXECUTIVE", 2412m, 2021 },
                    { 63, 3000m, new DateTime(2021, 10, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 1080m, 0m, 255m, "SALES", "10007562", "PETRA", "WINKLER", 12, "45698756", 10, "ZZ", "ACCOUNT EXECUTIVE", 2206m, 2021 },
                    { 62, 2995m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 1439m, 1018m, 70m, "SALES", "10007562", "CARLOS", "DIAZ", 18, "78455698", 9, "ZZ", "SALES MANAGER", 0m, 2021 },
                    { 61, 2995m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 1338m, "SALES", "10007562", "CARLOS", "DIAZ", 18, "78455698", 10, "ZZ", "SALES MANAGER", 0m, 2021 },
                    { 60, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1139m, 233m, 1869m, "OPERATION", "10007661", "LARS", "PETERSON", 6, "89564785", 5, "A", "CARGO ASSISTANT", 1588m, 2021 },
                    { 59, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 614m, 0m, "OPERATION", "10007661", "LARS", "PETERSON", 6, "89564785", 6, "A", "CARGO ASSISTANT", 1382m, 2021 },
                    { 58, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1592m, 0m, 1627m, "OPERATION", "10007661", "LARS", "PETERSON", 12, "89564785", 7, "D", "HEAD OF CARGO", 0m, 2021 },
                    { 57, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 60m, 539m, "OPERATION", "10007661", "LARS", "PETERSON", 12, "89564785", 8, "D", "HEAD OF CARGO", 571m, 2021 },
                    { 56, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1981m, 810m, 1247m, "OPERATION", "10007661", "LARS", "PETERSON", 18, "89564785", 9, "C", "CARGO MANAGER", 68m, 2021 },
                    { 55, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 579m, "OPERATION", "10007661", "LARS", "PETERSON", 18, "89564785", 10, "C", "CARGO MANAGER", 0m, 2021 },
                    { 54, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 195m, 427m, 0m, "CUSTOMER CARE", "10007759", "PETER", "WILSON", 6, "65897456", 5, "A", "CUSTOMER ASSISTANT", 302m, 2021 },
                    { 53, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 902m, "CUSTOMER CARE", "10007759", "PETER", "WILSON", 6, "65897456", 6, "A", "CUSTOMER ASSISTANT", 604m, 2021 },
                    { 74, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1989m, 379m, 856m, "OPERATION", "10007365", "SVEN", "PETERSEN", 18, "23654125", 9, "C", "CARGO MANAGER", 0m, 2021 },
                    { 52, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1589m, 176m, 655m, "CUSTOMER CARE", "10007759", "PETER", "WILSON", 14, "65897456", 7, "D", "CUSTOMER DIRECTOR", 329m, 2021 },
                    { 75, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 791m, 620m, "OPERATION", "10007365", "SVEN", "PETERSEN", 12, "23654125", 8, "D", "HEAD OF CARGO", 4680m, 2021 },
                    { 77, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 703m, 414m, 1526m, "OPERATION", "10007365", "SVEN", "PETERSEN", 6, "23654125", 6, "A", "CARGO ASSISTANT", 5092m, 2021 },
                    { 98, 2995m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 1995m, 1945m, 0m, "SALES", "10006971", "GUYLÈNE", "NODIER", 18, "96314785", 9, "ZZ", "SALES MANAGER", 0m, 2021 },
                    { 97, 2995m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 740m, "SALES", "10006971", "GUYLÈNE", "NODIER", 18, "96314785", 10, "ZZ", "SALES MANAGER", 9214m, 2021 },
                    { 96, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 513m, "OPERATION", "10007070", "MICHAEL", "BJÖRN", 6, "65478215", 5, "A", "CARGO ASSISTANT", 9008m, 2021 },
                    { 95, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1626m, 175m, 920m, "OPERATION", "10007070", "MICHAEL", "BJÖRN", 6, "65478215", 6, "A", "CARGO ASSISTANT", 0m, 2021 },
                    { 94, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 1678m, "OPERATION", "10007070", "MICHAEL", "BJÖRN", 12, "65478215", 7, "D", "HEAD OF CARGO", 8595m, 2021 },
                    { 93, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1242m, 1184m, 0m, "OPERATION", "10007070", "MICHAEL", "BJÖRN", 12, "65478215", 8, "D", "HEAD OF CARGO", 0m, 2021 },
                    { 92, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 1965m, 632m, "OPERATION", "10007070", "MICHAEL", "BJÖRN", 18, "65478215", 9, "C", "CARGO MANAGER", 8183m, 2021 },
                    { 91, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1173m, 0m, 26m, "OPERATION", "10007070", "MICHAEL", "BJÖRN", 18, "65478215", 10, "C", "CARGO MANAGER", 7977m, 2021 },
                    { 90, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 1069m, 1743m, "CUSTOMER CARE", "10007168", "CHERYL", "SAYLOR", 6, "16547895", 5, "A", "CUSTOMER ASSISTANT", 0m, 2021 },
                    { 89, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 773m, 386m, 0m, "CUSTOMER CARE", "10007168", "CHERYL", "SAYLOR", 6, "16547895", 6, "A", "CUSTOMER ASSISTANT", 7565m, 2021 },
                    { 88, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 348m, 401m, 223m, "CUSTOMER CARE", "10007168", "CHERYL", "SAYLOR", 14, "16547895", 7, "D", "CUSTOMER DIRECTOR", 7359m, 2021 },
                    { 87, 3200m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 307m, 551m, "CUSTOMER CARE", "10007168", "CHERYL", "SAYLOR", 14, "16547895", 8, "D", "CUSTOMER DIRECTOR", 7153m, 2021 },
                    { 86, 5000m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1089m, 0m, 1590m, "CUSTOMER CARE", "10007168", "CHERYL", "SAYLOR", 18, "16547895", 9, "D", "CUSTOMER DIRECTOR", 0m, 2021 },
                    { 85, 5000m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 1012m, 373m, "CUSTOMER CARE", "10007168", "CHERYL", "SAYLOR", 18, "16547895", 10, "D", "CUSTOMER DIRECTOR", 6741m, 2021 },
                    { 84, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 1723m, 0m, 770m, "MARKETING", "10007234", "BEATE", "VILEId", 11, "64216697", 5, "ZZ", "MARKETING ASSISTANT", 0m, 2021 },
                    { 83, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 1757m, 1802m, 0m, "MARKETING", "10007234", "BEATE", "VILEId", 11, "64216697", 6, "ZZ", "MARKETING ASSISTANT", 6328m, 2021 }
                });

            migrationBuilder.InsertData(
                table: "tblEmployee",
                columns: new[] { "Id", "Base Salary", "Begin Date", "Birthday", "Commission", "Compensation Bonus", "Contributions", "Division", "Employee Code", "Employee Name", "Employee Surname", "Grade", "Identification Number", "Month", "Office", "Position", "Production Bonus", "Year" },
                values: new object[,]
                {
                    { 82, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 774m, 451m, "SALES", "10007234", "BEATE", "VILEId", 12, "64216697", 7, "ZZ", "ACCOUNT EXECUTIVE", 6122m, 2021 },
                    { 81, 3000m, new DateTime(2021, 10, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 1162m, 0m, 284m, "SALES", "10007234", "BEATE", "VILEId", 12, "64216697", 10, "ZZ", "ACCOUNT EXECUTIVE", 5916m, 2021 },
                    { 80, 2995m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 264m, 111m, "SALES", "10007267", "ELIO", "ROSSI", 18, "54326874", 9, "ZZ", "SALES MANAGER", 0m, 2021 },
                    { 79, 2995m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 1196m, 432m, 402m, "SALES", "10007267", "ELIO", "ROSSI", 18, "54326874", 10, "ZZ", "SALES MANAGER", 5504m, 2021 },
                    { 78, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 5m, 0m, "OPERATION", "10007365", "SVEN", "PETERSEN", 6, "23654125", 5, "A", "CARGO ASSISTANT", 5298m, 2021 },
                    { 76, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1935m, 600m, 1363m, "OPERATION", "10007365", "SVEN", "PETERSEN", 12, "23654125", 7, "D", "HEAD OF CARGO", 0m, 2021 },
                    { 51, 3200m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1550m, 0m, 0m, "CUSTOMER CARE", "10007759", "PETER", "WILSON", 14, "65897456", 8, "D", "CUSTOMER DIRECTOR", 1702m, 2021 },
                    { 50, 5000m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 340m, 233m, "CUSTOMER CARE", "10007759", "PETER", "WILSON", 18, "65897456", 9, "D", "CUSTOMER DIRECTOR", 96m, 2021 },
                    { 49, 5000m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1631m, 1849m, 122m, "CUSTOMER CARE", "10007759", "PETER", "WILSON", 18, "65897456", 10, "D", "CUSTOMER DIRECTOR", 0m, 2021 },
                    { 22, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 770m, 0m, 1292m, "OPERATION", "10008251", "CHARLOTTE", "COOPER", 12, "46028356", 7, "D", "HEAD OF CARGO", 370m, 2021 },
                    { 21, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1743m, 830m, 823m, "OPERATION", "10008251", "CHARLOTTE", "COOPER", 12, "46028356", 8, "D", "HEAD OF CARGO", 0m, 2021 },
                    { 20, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 1564m, 697m, "OPERATION", "10008251", "CHARLOTTE", "COOPER", 18, "46028356", 9, "C", "CARGO MANAGER", 0m, 2021 },
                    { 19, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1545m, 0m, 234m, "OPERATION", "10008251", "CHARLOTTE", "COOPER", 18, "46028356", 10, "C", "CARGO MANAGER", 1603m, 2021 },
                    { 18, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 801m, 2475m, 0m, "CUSTOMER CARE", "10001001", "ANN", "WHITAKER", 6, "8563217", 5, "A", "CUSTOMER ASSISTANT", 1852m, 2021 },
                    { 17, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 896m, 2475m, 0m, "CUSTOMER CARE", "10001001", "ANN", "WHITAKER", 6, "8563217", 6, "A", "CUSTOMER ASSISTANT", 1899m, 2021 },
                    { 16, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1944m, 998m, 600m, "CUSTOMER CARE", "10001001", "ANN", "WHITAKER", 14, "8563217", 7, "D", "CUSTOMER DIRECTOR", 0m, 2021 },
                    { 15, 3200m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1845m, 998m, 600m, "CUSTOMER CARE", "10001001", "ANN", "WHITAKER", 14, "8563217", 8, "D", "CUSTOMER DIRECTOR", 0m, 2021 },
                    { 14, 5000m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 2677m, 3200m, 1000m, "CUSTOMER CARE", "10001001", "ANN", "WHITAKER", 18, "8563217", 9, "D", "CUSTOMER DIRECTOR", 0m, 2021 },
                    { 13, 5000m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 2478m, 3200m, 1000m, "CUSTOMER CARE", "10001001", "ANN", "WHITAKER", 18, "8563217", 10, "D", "CUSTOMER DIRECTOR", 0m, 2021 },
                    { 12, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 2365m, 0m, "MARKETING", "10023001", "JANE", "DOE", 11, "1989863", 5, "ZZ", "MARKETING ASSISTANT", 2666m, 2021 },
                    { 11, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 2365m, 0m, "MARKETING", "10023001", "JANE", "DOE", 11, "1989863", 6, "ZZ", "MARKETING ASSISTANT", 2666m, 2021 },
                    { 10, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 2365m, 600m, "SALES", "10023001", "JANE", "DOE", 12, "1989863", 7, "ZZ", "ACCOUNT EXECUTIVE", 3950m, 2021 },
                    { 9, 3000m, new DateTime(2021, 10, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 600m, "SALES", "10023001", "JANE", "DOE", 12, "1989863", 10, "ZZ", "ACCOUNT EXECUTIVE", 4000m, 2021 },
                    { 8, 2995m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 3200m, 2340m, "SALES", "10023000", "KALI", "PRASAD", 18, "14598756", 9, "ZZ", "SALES MANAGER", 3200m, 2021 },
                    { 7, 2995m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 3200m, 2100m, "SALES", "10023000", "KALI", "PRASAD", 18, "14598756", 10, "ZZ", "SALES MANAGER", 3000m, 2021 },
                    { 6, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 5000m, 1500m, 1600m, "OPERATION", "10001222", "MIKE", "JAMES", 6, "45642132", 5, "A", "CARGO ASSISTANT", 1800m, 2021 },
                    { 5, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 4500m, 1500m, 1450m, "OPERATION", "10001222", "MIKE", "JAMES", 6, "45642132", 6, "A", "CARGO ASSISTANT", 2000m, 2021 },
                    { 4, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 998m, 600m, "OPERATION", "10001222", "MIKE", "JAMES", 12, "45642132", 7, "D", "HEAD OF CARGO", 0m, 2021 },
                    { 3, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 998m, 600m, "OPERATION", "10001222", "MIKE", "JAMES", 12, "45642132", 8, "D", "HEAD OF CARGO", 0m, 2021 },
                    { 2, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 3263m, 1000m, "OPERATION", "10001222", "MIKE", "JAMES", 18, "45642132", 9, "C", "CARGO MANAGER", 0m, 2021 },
                    { 23, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 355m, 1554m, "OPERATION", "10008251", "CHARLOTTE", "COOPER", 6, "46028356", 6, "A", "CARGO ASSISTANT", 0m, 2021 },
                    { 24, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1725m, 0m, 1907m, "OPERATION", "10008251", "CHARLOTTE", "COOPER", 6, "46028356", 5, "A", "CARGO ASSISTANT", 510m, 2021 },
                    { 25, 2995m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 1020m, "SALES", "10008153", "SHELLEY", "BURKE", 18, "32569872", 10, "ZZ", "SALES MANAGER", 420m, 2021 },
                    { 26, 2995m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 756m, 0m, 1812m, "SALES", "10008153", "SHELLEY", "BURKE", 18, "32569872", 9, "ZZ", "SALES MANAGER", 1035m, 2021 },
                    { 48, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 1479m, 0m, "MARKETING", "10007825", "IAN", "DEVLING", 11, "32658749", 5, "ZZ", "MARKETING ASSISTANT", 1910m, 2021 },
                    { 47, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 982m, 132m, 1575m, "MARKETING", "10007825", "IAN", "DEVLING", 11, "32658749", 6, "ZZ", "MARKETING ASSISTANT", 1865m, 2021 },
                    { 46, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 957m, 344m, "SALES", "10007825", "IAN", "DEVLING", 12, "32658749", 7, "ZZ", "ACCOUNT EXECUTIVE", 978m, 2021 },
                    { 45, 3000m, new DateTime(2021, 10, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 1558m, 0m, 949m, "SALES", "10007825", "IAN", "DEVLING", 12, "32658749", 10, "ZZ", "ACCOUNT EXECUTIVE", 1474m, 2021 },
                    { 44, 2995m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 189m, 457m, 1621m, "SALES", "10007858", "MAYUMI", "OHNO", 18, "32658954", 9, "ZZ", "SALES MANAGER", 1795m, 2021 },
                    { 43, 2995m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 0m, "SALES", "10007858", "MAYUMI", "OHNO", 18, "32658954", 10, "ZZ", "SALES MANAGER", 0m, 2021 },
                    { 42, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 1473m, 365m, 1600m, "OPERATION", "10007956", "ANTONIO", "SAAVEDRA", 6, "23658954", 5, "A", "CARGO ASSISTANT", 1516m, 2021 },
                    { 41, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 443m, "OPERATION", "10007956", "ANTONIO", "SAAVEDRA", 6, "23658954", 6, "A", "CARGO ASSISTANT", 1940m, 2021 }
                });

            migrationBuilder.InsertData(
                table: "tblEmployee",
                columns: new[] { "Id", "Base Salary", "Begin Date", "Birthday", "Commission", "Compensation Bonus", "Contributions", "Division", "Employee Code", "Employee Name", "Employee Surname", "Grade", "Identification Number", "Month", "Office", "Position", "Production Bonus", "Year" },
                values: new object[,]
                {
                    { 40, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 131m, 1604m, 694m, "OPERATION", "10007956", "ANTONIO", "SAAVEDRA", 12, "23658954", 7, "D", "HEAD OF CARGO", 0m, 2021 },
                    { 39, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 358m, 12m, "OPERATION", "10007956", "ANTONIO", "SAAVEDRA", 12, "23658954", 8, "D", "HEAD OF CARGO", 1103m, 2021 },
                    { 99, 3000m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 505m, "SALES", "10006971", "GUYLÈNE", "NODIER", 18, "96314785", 8, "ZZ", "SALES MANAGER", 9626m, 2021 },
                    { 38, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 422m, 469m, 0m, "OPERATION", "10007956", "ANTONIO", "SAAVEDRA", 18, "23658954", 9, "C", "CARGO MANAGER", 0m, 2021 },
                    { 36, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1172m, 209m, 1921m, "CUSTOMER CARE", "10008055", "YOSHI", "NAGASE", 6, "65874965", 5, "A", "CUSTOMER ASSISTANT", 1496m, 2021 },
                    { 35, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1178m, 608m, 0m, "CUSTOMER CARE", "10008055", "YOSHI", "NAGASE", 6, "65874965", 6, "A", "CUSTOMER ASSISTANT", 0m, 2021 },
                    { 34, 2799m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 956m, 0m, 754m, "CUSTOMER CARE", "10008055", "YOSHI", "NAGASE", 14, "65874965", 7, "D", "CUSTOMER DIRECTOR", 1675m, 2021 },
                    { 33, 3200m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 751m, "CUSTOMER CARE", "10008055", "YOSHI", "NAGASE", 14, "65874965", 8, "D", "CUSTOMER DIRECTOR", 1069m, 2021 },
                    { 32, 5000m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 762m, 0m, 97m, "CUSTOMER CARE", "10008055", "YOSHI", "NAGASE", 18, "65874965", 9, "D", "CUSTOMER DIRECTOR", 1072m, 2021 },
                    { 31, 5000m, new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1992, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1380m, 1396m, 0m, "CUSTOMER CARE", "10008055", "YOSHI", "NAGASE", 18, "65874965", 10, "D", "CUSTOMER DIRECTOR", 0m, 2021 },
                    { 30, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 0m, 0m, "MARKETING", "10008120", "REGINA", "MURPHY", 11, "56985647", 5, "ZZ", "MARKETING ASSISTANT", 835m, 2021 },
                    { 29, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 1579m, 0m, 522m, "MARKETING", "10008120", "REGINA", "MURPHY", 11, "56985647", 6, "ZZ", "MARKETING ASSISTANT", 552m, 2021 },
                    { 28, 2799m, new DateTime(2021, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 1405m, 0m, 888m, "SALES", "10008120", "REGINA", "MURPHY", 12, "56985647", 7, "ZZ", "ACCOUNT EXECUTIVE", 425m, 2021 },
                    { 27, 3000m, new DateTime(2021, 10, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1978, 11, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 124m, 1370m, "SALES", "10008120", "REGINA", "MURPHY", 12, "56985647", 10, "ZZ", "ACCOUNT EXECUTIVE", 0m, 2021 },
                    { 37, 2799m, new DateTime(2021, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1990, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), 0m, 187m, 1645m, "OPERATION", "10007956", "ANTONIO", "SAAVEDRA", 18, "23658954", 10, "C", "CARGO MANAGER", 1023m, 2021 },
                    { 100, 3000m, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1980, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), 1720m, 1028m, 1756m, "SALES", "10006971", "GUYLÈNE", "NODIER", 18, "96314785", 7, "ZZ", "SALES MANAGER", 0m, 2021 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblEmployee");
        }
    }
}
