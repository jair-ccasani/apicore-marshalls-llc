﻿using Prueba.Marshalls.Domain.Base;
using Prueba.Marshalls.Domain.Entities.Employees;
using Prueba.Marshalls.Domain.Interfaces;
using Prueba.Marshalls.Infraestructure.Data.Repositories;
using System.Threading.Tasks;

namespace Prueba.Marshalls.Infraestructure.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BDTestContext _dbContext;
        private readonly IEmployeeRepository _employeeRepository;

        public UnitOfWork(BDTestContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEmployeeRepository EmployeeRepository => _employeeRepository ?? new EmployeeRepository(_dbContext);

        public IRepository<T> Repository<T>() where T : BaseEntity
        {
            return new BaseRepository<T>(_dbContext);
        }

        public async Task SaveChangesAsync()
        {
            await  _dbContext.SaveChangesAsync();
        }
    }
}
