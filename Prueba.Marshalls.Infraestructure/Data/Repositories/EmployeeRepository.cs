﻿using Prueba.Marshalls.Domain.Entities.Employees;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Prueba.Marshalls.Infraestructure.Data.Repositories
{
    public class EmployeeRepository : BaseRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(BDTestContext dbContext) : base(dbContext)
        {
        }

        public async Task BulkInsertAsync(IEnumerable<Employee> employees)
        {
           await _dbSet.AddRangeAsync(employees);
        }

        public async Task BulkUpdateAsync(IEnumerable<Employee> employees)
        {
            await Task.Run(()=>_dbSet.UpdateRange(employees));
        }
    }
}
