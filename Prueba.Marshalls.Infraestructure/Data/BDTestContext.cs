﻿using Microsoft.EntityFrameworkCore;
using Prueba.Marshalls.Domain.Entities.Employees;
using System.Reflection;

#nullable disable

namespace Prueba.Marshalls.Infraestructure.Data
{
    public partial class BDTestContext : DbContext
    {
        public BDTestContext()
        {
        }

        public BDTestContext(DbContextOptions<BDTestContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Employee> Salaries { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
