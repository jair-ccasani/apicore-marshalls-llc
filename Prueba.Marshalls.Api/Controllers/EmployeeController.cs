﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Prueba.Marshalls.Api.DTOs.Employees;
using Prueba.Marshalls.Api.Services.Employees;
using Prueba.Marshalls.Domain.Entities.Employees;
using Prueba.Marshalls.Domain.Shared;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prueba.Marshalls.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeService _service;
        private readonly IMapper _mapper;
        private readonly ILogger<EmployeeController> _logger;
        public EmployeeController(ILogger<EmployeeController> logger, EmployeeService service, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }
        // GET: api/<EmployeeController>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetEmployeeRequest request)
        {
  

            var Employees =  request.employeeFilterBy switch
            {
                EmployeeFilterBy.Same_Office_And_Grade => await _service.GetEmployeeSameOfficeAndGradeAsync(request),
                EmployeeFilterBy.All_Offices_And_With_The_Same_Grade => await _service.GetEmployeeAllOfficesAndWithTheSameGradeAsync(request),
                EmployeeFilterBy.Same_Position_And_Grade => await _service.GetEmployeeSamePositionAndGradeAsync(request),
                EmployeeFilterBy.All_Positions_And_With_the_Same_Grade => await _service.GetEmployeeAllPositionsAndWiththeSameGradeAsync(request),
                _ => await _service.GetLastSalaryByEmployee(),
            };
        
            var EmployeesDto = _mapper.Map<IEnumerable<EmployeeInformationDTO>>(Employees);
            return Ok(EmployeesDto);
        }

        // POST api/<EmployeeController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] List<EmployeeRequest> request)
        {
            var Employees = _mapper.Map<IEnumerable<Employee>>(request);
            await _service.AddBulkAsync(Employees);
            return Ok();
        }

        // PUT api/<EmployeeController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] List<EmployeeRequest> request)
        {
            var Employees = _mapper.Map<IEnumerable<Employee>>(request);
            await _service.UpdateBulkAsync(Employees);
            return Ok();
        }
    }
}
