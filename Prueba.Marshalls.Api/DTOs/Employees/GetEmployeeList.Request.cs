﻿using Prueba.Marshalls.Domain.Shared;

namespace Prueba.Marshalls.Api.DTOs.Employees
{
    public class GetEmployeeRequest
    {
        public EmployeeFilterBy employeeFilterBy { get; set; }
        public string Office { get; set; }
        public string Position { get; set; }
        public int? Grade { get; set; }
    }
}
