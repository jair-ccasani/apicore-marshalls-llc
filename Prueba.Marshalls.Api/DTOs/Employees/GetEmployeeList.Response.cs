﻿using System;

namespace Prueba.Marshalls.Api.DTOs.Employees
{
    public class EmployeeInformationDTO
    {
        public int Id { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeFullname { get; set; }
        public string Division { get; set; }
        public string Position { get; set; }
        public int? Grade { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal TotalSalary { get; set; }
        public string Office { get; set; }
    }
}
