﻿using AutoMapper;
using Prueba.Marshalls.Api.DTOs.Employees;
using Prueba.Marshalls.Domain.Entities.Employees;

namespace Prueba.Marshalls.Api.Mappings
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Employee, EmployeeInformationDTO>()
                .ForMember(
                    dto => dto.EmployeeFullname,
                    entity => entity.MapFrom(source => 
                                            new EmployeeValueObject(source.EmployeeName
                                            ,source.EmployeeSurname)
                                            .EmployeeFullname
                                            )
                ).ForMember(
                    dto => dto.TotalSalary,
                    entity => entity.MapFrom(source =>
                                            new EmployeeValueObject(source.BaseSalary
                                                                    ,source.Commission
                                                                    ,source.ProductionBonus
                                                                    ,source.CompensationBonus
                                                                    ,source.Contributions)
                                                                    .TotalSalary
                                            )
                );


            CreateMap<EmployeeRequest, Employee>();
        }
    }
}
