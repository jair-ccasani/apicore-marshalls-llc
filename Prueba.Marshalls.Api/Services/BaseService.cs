﻿using Prueba.Marshalls.Domain.Interfaces;

namespace Prueba.Marshalls.Api.Services
{
    public class BaseService
    {
        public BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        protected internal IUnitOfWork UnitOfWork { get; set; }
    }
}
