﻿using Microsoft.EntityFrameworkCore;
using Prueba.Marshalls.Api.DTOs.Employees;
using Prueba.Marshalls.Domain.Base;
using Prueba.Marshalls.Domain.Entities.Employees;
using Prueba.Marshalls.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prueba.Marshalls.Api.Services.Employees
{
    public class EmployeeService : BaseService
    {
        public EmployeeService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task AddBulkAsync(IEnumerable<Employee> employees)
        {
            var repository = UnitOfWork.Repository<Employee>();

            var RepeatRequestData = ValidateRepeatYearAndMonth(employees);

            if (RepeatRequestData != null)
            {
                throw new BusinessException(
                    string.Format(EmployeeResource.msg_Repeat_Year_and_Month_Salary_Request
                    ,RepeatRequestData.Year, RepeatRequestData.Month));
            }
            //Validate if exists Employee
            var employeeInDatabase = await repository.GetAsync(_ => 
                                                                _.EmployeeName.Trim().ToLower().Equals(employees.First().EmployeeName.Trim().ToLower()) 
                                                                && _.EmployeeSurname.Trim().ToLower().Equals(employees.First().EmployeeSurname.Trim().ToLower()));
            if (employeeInDatabase != null)
            {
                throw new BusinessException(string.Format(EmployeeResource.msg_Exists_Employee, $"{employeeInDatabase.EmployeeName} {employeeInDatabase.EmployeeSurname}"));
            }

            employeeInDatabase = await repository.GetAsync(_ => _.EmployeeCode.Trim().ToLower().Equals(employees.First().EmployeeCode.Trim().ToLower()));
            if (employeeInDatabase != null)
            {
                throw new BusinessException(string.Format(EmployeeResource.msg_Exists_Employee_Code, employeeInDatabase.EmployeeCode));
                
            }

            await UnitOfWork.EmployeeRepository.BulkInsertAsync(employees);
            await  UnitOfWork.SaveChangesAsync();

        }

        public async Task UpdateBulkAsync( IEnumerable<Employee> employees)
        {
            var repeatYearAndMonthInRequestData = ValidateRepeatYearAndMonth(employees);

            if (repeatYearAndMonthInRequestData != null)
            {
                throw new BusinessException(
                    string.Format(EmployeeResource.msg_Repeat_Year_and_Month_Salary_Request
                    , repeatYearAndMonthInRequestData.Year, repeatYearAndMonthInRequestData.Month));
            }

            var validateEmployeeCodeAndNameChanges = await ValidateEmployeeCodeAndNameChanges(employees);
            if (!validateEmployeeCodeAndNameChanges.Item1)
            {
                throw new BusinessException(validateEmployeeCodeAndNameChanges.Item2);
            }


            var validateRepeatYearAndMonthUpdate = await ValidateRepeatYearAndMonthUpdate(employees);
            if (!validateRepeatYearAndMonthUpdate.Item1)
            {
                throw new BusinessException(validateEmployeeCodeAndNameChanges.Item2);
            }
            

            var employeesInsert = employees.Where(x => x.Id == 0);
            var employeesUpdate= employees.Where(x => x.Id > 0);
            await UnitOfWork.EmployeeRepository.BulkInsertAsync(employeesInsert);
            await UnitOfWork.EmployeeRepository.BulkUpdateAsync(employeesUpdate);
            await UnitOfWork.SaveChangesAsync();

        }

        public async Task<IQueryable<Employee>> GetEmployees()
        {
            var repository = UnitOfWork.Repository<Employee>();
            return await repository.ListAllAsync();
        }
        public async Task<IQueryable<Employee>> GetLastSalaryByEmployee()
        {
            var repository = UnitOfWork.Repository<Employee>();
            var queryBase = await repository.ListAllAsync();

            var filterLastSalaryByEmployee = queryBase.GroupBy(x => x.EmployeeCode).Select(x => new Employee
            {
                EmployeeCode = x.Max(x => x.EmployeeCode),
                Year = x.Max(x => x.Year),
                Month = x.Max(x => x.Month)
            }).AsQueryable();

            return queryBase.Where(x => filterLastSalaryByEmployee.Any(y => y.EmployeeCode == x.EmployeeCode 
                                                                        && y.Year == x.Year 
                                                                        && y.Month == x.Month       
                                            )).AsQueryable();

                

        }
        public async Task<IQueryable<Employee>> GetEmployeeSameOfficeAndGradeAsync(GetEmployeeRequest request)
        {
            var BaseQuery = await GetLastSalaryByEmployee();
           return BaseQuery.Where(x =>
                                    x.Office.Trim().ToLower().Equals(request.Office.Trim().ToLower())
                                    && x.Grade == request.Grade
                                    ).AsQueryable();


        }

        public async Task<IQueryable<Employee>> GetEmployeeAllOfficesAndWithTheSameGradeAsync(GetEmployeeRequest request)
        {
            var BaseQuery = await GetLastSalaryByEmployee();
            return BaseQuery.Where(x => x.Grade == request.Grade
                                     ).OrderBy(x => x.Office).AsQueryable();

        }

        public async Task<IQueryable<Employee>> GetEmployeeSamePositionAndGradeAsync(GetEmployeeRequest request)
        {
            var BaseQuery = await GetLastSalaryByEmployee();
            return BaseQuery.Where(x =>
                                     x.Position.Trim().ToLower().Equals(request.Position.Trim().ToLower())
                                     && x.Grade == request.Grade
                                     ).AsQueryable();

        }

        public async Task<IQueryable<Employee>> GetEmployeeAllPositionsAndWiththeSameGradeAsync(GetEmployeeRequest request)
        {
            var BaseQuery = await GetLastSalaryByEmployee();
            return BaseQuery.Where(x =>x.Grade == request.Grade
                                     ).OrderBy(x => x.Position).AsQueryable();

        }

        private Employee ValidateRepeatYearAndMonth(IEnumerable<Employee> employees)
        {
            var dataFilter = employees.Select(x => $"{x.Year}{x.Month}").ToList();
            return employees.Where(x => dataFilter.Contains($"{x.Year}{x.Month}")).FirstOrDefault();
        }

        private async Task<(bool,string)> ValidateEmployeeCodeAndNameChanges(IEnumerable<Employee> employeesToUpdate)
        {
            (bool, string) validateResult = (true, "");
            var repository = UnitOfWork.Repository<Employee>();

            var employeeInDatabase = await repository.GetAsync(_ => _.Id == employeesToUpdate.First().Id);

            //validate employee code changes 
            if (!employeeInDatabase.EmployeeCode.Trim().ToLower().Equals(employeesToUpdate.First().EmployeeCode.Trim().ToLower()))
            {
                //validate if new employeeCode exist en database
                var validateNewEmployeeCode = await repository.GetAsync(_ => _.EmployeeCode == employeesToUpdate.First().EmployeeCode);
                if(validateNewEmployeeCode != null)
                {
                    validateResult.Item1 = false;
                    validateResult.Item2 = String.Format(EmployeeResource.msg_Exists_Employee_Code,validateNewEmployeeCode.EmployeeCode);
                }
            
            }

            if (!employeeInDatabase.EmployeeName.Trim().ToLower().Equals(employeesToUpdate.First().EmployeeName.Trim().ToLower()) ||
                !employeeInDatabase.EmployeeSurname.Trim().ToLower().Equals(employeesToUpdate.First().EmployeeSurname.Trim().ToLower()))
            {
                //validate if new employee full name exist en database
                var validateNewEmployeeFullname = await repository.GetAsync(_ =>
                                                                _.EmployeeName.Trim().ToLower().Equals(employeesToUpdate.First().EmployeeName.Trim().ToLower())
                                                                && _.EmployeeSurname.Trim().ToLower().Equals(employeesToUpdate.First().EmployeeSurname.Trim().ToLower()));
                if (validateNewEmployeeFullname != null)
                {
                    validateResult.Item1 = false;
                    validateResult.Item2 += validateResult.Item2.Length > 0 ? "\n\n" : "";
                    validateResult.Item2 += string.Format(EmployeeResource.msg_Exists_Employee, $"{employeeInDatabase.EmployeeName} {employeeInDatabase.EmployeeSurname}");
                }

            }

            return validateResult;
        }
        private async Task<(bool, string)> ValidateRepeatYearAndMonthUpdate(IEnumerable<Employee> employeesToUpdate)
        {
            (bool, string) validateResult = (true, "");
            var repository = UnitOfWork.Repository<Employee>();
            var employeeIdsToUpdate = employeesToUpdate.Where(x => x.Id > 0).ToList();

            var employeeInDatabase = await repository.GetAsync(_ => _.Id == employeesToUpdate.First().Id);
            var employeesInDatabase = await repository.ListAsync(_ => _.EmployeeCode.Equals(employeeInDatabase.EmployeeCode) && !employeeIdsToUpdate.Select(x => x.Id).Contains(_.Id));

            var allEmployeesToInsertAndUpdate = employeesToUpdate.Concat(employeesInDatabase);

            var RepeatRequestData =  ValidateRepeatYearAndMonth(allEmployeesToInsertAndUpdate);

            if(RepeatRequestData != null)
            {
                validateResult.Item1 = false;
                validateResult.Item2 = string.Format(EmployeeResource.msg_Repeat_Year_and_Month_Salary_Request
                    , RepeatRequestData.Year, RepeatRequestData.Month);
            }

            return validateResult;
        }
    }
}
