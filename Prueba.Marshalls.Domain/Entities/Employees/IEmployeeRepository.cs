﻿using Prueba.Marshalls.Domain.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Prueba.Marshalls.Domain.Entities.Employees
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        Task BulkInsertAsync(IEnumerable<Employee> employees);
        Task BulkUpdateAsync(IEnumerable<Employee> employees);
    }
}
