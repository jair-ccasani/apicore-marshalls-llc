﻿namespace Prueba.Marshalls.Domain.Entities.Employees
{
    public class EmployeeValueObject
    {
        public EmployeeValueObject(decimal? BaseSalary
           , decimal? Commission
           , decimal? ProductionBonus
            ,decimal? CompensationBonus
            , decimal? Contributions)
        {
            OtherIncome = ((BaseSalary ?? 0 + Commission ?? 0) * 8)/100;
            TotalSalary = BaseSalary ?? 0 + ProductionBonus ?? 0 
                            + ((CompensationBonus ?? 0 * 75) /100) 
                            +(OtherIncome - Contributions ?? 0);
        }

        public EmployeeValueObject(string EmployeeName, string EmployeeSurname)
        {
            EmployeeFullname = $"{EmployeeName} {EmployeeSurname}";
        }

        public decimal OtherIncome { get; private set; }
        public decimal TotalSalary { get; private set; }
        public string EmployeeFullname { get; private set; }
    }
}
