﻿using Prueba.Marshalls.Domain.Base;
using Prueba.Marshalls.Domain.Entities.Employees;
using System.Threading.Tasks;

namespace Prueba.Marshalls.Domain.Interfaces
{
    public  interface IUnitOfWork
    {
        Task SaveChangesAsync();
        IRepository<T> Repository<T>() where T : BaseEntity;
        IEmployeeRepository EmployeeRepository { get; }
    }
}
