﻿using MediatR;
using System;

namespace Prueba.Marshalls.Domain.Base
{
    public abstract class BaseDomainEvent : INotification
    {
        public virtual Guid EventId { get; protected set; } = Guid.NewGuid();
        public virtual DateTime CreatedOn { get; protected set; } = DateTime.UtcNow;
    }
}
