﻿using System.Collections.Generic;

namespace Prueba.Marshalls.Domain.Base
{
    public abstract class BaseEntity
    {
        public List<BaseDomainEvent> _events = new List<BaseDomainEvent> ();

        protected void AddEvent(BaseDomainEvent @event)
        {
            _events.Add(@event);
        }

        protected void RemoveEvent(BaseDomainEvent @event)
        {
            _events.Remove(@event);
        }
    }

    public abstract class BaseEntity<TKey> : BaseEntity
    {
        public TKey Id { get; set; }
    }
}
