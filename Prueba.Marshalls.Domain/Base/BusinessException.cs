﻿using System;

namespace Prueba.Marshalls.Domain.Base
{
    public class BusinessException : Exception
    {
        public BusinessException()
        {

        }

        public BusinessException(string message) : base(message)
        {

        }
    }
}
