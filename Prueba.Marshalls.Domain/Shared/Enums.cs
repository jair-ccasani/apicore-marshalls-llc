﻿namespace Prueba.Marshalls.Domain.Shared
{
    public enum EmployeeFilterBy
    {
        
        Same_Office_And_Grade = 1,
        All_Offices_And_With_The_Same_Grade,
        Same_Position_And_Grade,
        All_Positions_And_With_the_Same_Grade
    }
}
